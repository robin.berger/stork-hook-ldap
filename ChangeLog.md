* 2 [build] andrei

    Add coverage reporting to unit tests.
    (Gitlab #1174)

* 1 [func] slawek

    Implemented the initial version of the LDAP hook for Stork server.
    (Gitlab #638)
