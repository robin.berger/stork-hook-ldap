# Stork LDAP hook

The repository contains a hook (plugin, extension) for
[Stork server](https://gitlab.isc.org/isc-projects/stork) that allows to
authenticate users using LDAP (Lightweight Directory Access Protocol)
credentials.

The hook is in an **experimental** phase.

## Installation from the official package

The hook package should be available soon in our CloudSmith repository.

<!--Download the [PACKAGE NAME] where [XXX] is the Stork server version from the
[CloudSmith repository]([LINK TO REPO]). Install it using the package manager
and restart the Stork server.-->

The installed hook is located in the `/var/lib/stork-server/hooks` directory.

## Configuration

You can display the full list of available settings running the Stork server
with the `--help` flag. All below CLI flags must be prefixed with the hook name
ending with a dot. Below it is `ldap.`.

The LDAP hook requires several mandatory parameters:

- `--ldap.url` - The LDAP server access URL (use ldaps:// protocol to connect
  over TLS).
- `--ldap.root` - The LDAP root.
- `--ldap.bind-username` - The maintenance username used to bind to the server
  for reading user profiles.
- `--ldap.bind-password` - The maintenance password used to bind to the server
  for reading user profiles.

The LDAP hook is able to remap the LDAP group membership to the Stork roles.
To enable this feature specify the `--ldap.map-groups` flag. If you want to
limit access to the Stork server only for particular users, you can provide the
name of the mandatory allow group using the ``--group-allow`` argument. Users
who don't belong to this group will be rejected to authorize. By default, Stork
maps the `stork-admin` LDAP group to the `admin` role and `stork-super-admin`
group to `super-admin` role. The groups may be overridden if needed.

This hook reads the user profile from the LDAP server and converts it to the
Stork-specific format. The default schema of user and group membership is
compliant with the
[RFC 4519 - Lightweight Directory Access Protocol (LDAP): Schema for User Applications](https://datatracker.ietf.org/doc/html/rfc4519).
You can override the schema and its details if needed.

## Installation from sources

The Golang is static-linked language. You must ensure that the Stork server
and the hook were compiled with the same compiler, toolchain, codebase, and
compilation flags.

Ensure the Go version specified in the `go.mod` file matches the version in the
Stork server `go.mod` file and the pseudo-version or relative path in the
`replace` in the `go.mod` file points to the proper Stork server codebase.

To compile the hook execute:

```
rake build
```

The hook will be created in the `build/` directory.

To install the plugin, move the file with the `.so` extension to the Stork
server hook directory. By default it is `/var/lib/stork-server/hooks`.

## Development

There are several Rake tasks for development-purposes:

- `rake lint` - lint the code using the built-in Go linter
- `rake unittest` - execute the unit tests
- `rake fmt` - format the code using the built-in Go linter

More tasks are available in the main Stork repository in the `hook:` namespace.
