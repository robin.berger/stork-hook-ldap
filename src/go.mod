module isc.org/stork-hook-ldap

go 1.21

require (
	github.com/go-ldap/ldap/v3 v3.4.4
	github.com/stretchr/testify v1.8.4
	isc.org/stork v0.0.0
)

require (
	github.com/Azure/go-ntlmssp v0.0.0-20220621081337-cb9428e4ac1e // indirect
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/go-asn1-ber/asn1-ber v1.5.4 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	golang.org/x/crypto v0.13.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)

replace isc.org/stork => gitlab.isc.org/isc-projects/stork/backend v1.12.1-0.20231010214245-da5baa8223cf
