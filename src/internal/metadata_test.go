package internal_test

import (
	"io"
	"testing"

	"github.com/stretchr/testify/require"
	"isc.org/stork-hook-ldap/internal"
)

// Checks if the label of the identifier form is returned.
func TestGetIdentifierFormLabel(t *testing.T) {
	// Arrange
	metadata := &internal.Metadata{}

	// Act & Assert
	require.Equal(t, "Username", metadata.GetIdentifierFormLabel())
}

// Checks if the label of the secret form is returned.
func TestGetSecretFormLabel(t *testing.T) {
	// Arrange
	metadata := &internal.Metadata{}

	// Act & Assert
	require.Equal(t, "Password", metadata.GetSecretFormLabel())
}

// Checks if the description is returned.
func TestGetDescription(t *testing.T) {
	// Arrange
	metadata := &internal.Metadata{}

	// Act & Assert
	require.NotEmpty(t, metadata.GetDescription())
}

// Checks if the ID is returned.
func TestGetID(t *testing.T) {
	// Arrange
	metadata := &internal.Metadata{}

	// Act & Assert
	require.Equal(t, "ldap", metadata.GetID())
}

// Checks if the icon is returned.
func TestGetIcon(t *testing.T) {
	// Arrange
	metadata := &internal.Metadata{}

	// Act
	icon, err := metadata.GetIcon()

	// Assert
	require.NoError(t, err)
	require.NotNil(t, icon)
	defer icon.Close()
	content, err := io.ReadAll(icon)
	require.NoError(t, err)
	require.NotEmpty(t, content)
}

// Checks if the name is returned.
func TestGetName(t *testing.T) {
	// Arrange
	metadata := &internal.Metadata{}

	// Act & Assert
	require.Equal(t, "LDAP", metadata.GetName())
}
